# Node.js Bamboo Plugin

Plugin for integrating [Node.js][node] with [Atlassian Bamboo][bamboo].

[node]: http://nodejs.org/
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

* node
* npm

## To do/ideas

* grunt task
* mocha test parsing task
* nodeunit test parsing task

Integrating mocha & bamboo with mocha-bamboo-reporter
=====================================================

A patched version of the Atlassian Bamboo Node.js plugin with support for this module exists [here](https://bitbucket.org/issacg/bamboo-nodejs-plugin/downloads#download-190726)
Install it on your bamboo server

Then, in your package.json file, add a devDependency for "mocha-bamboo-reporter", and a script "bamboo" as outlined below...

    package.json
    
    ...
    "devDependencies": {
        ...
        "mocha": ">=1.8.1",
        "mocha-bamboo-reporter": "*"
    }
    
    "scripts": {
        ...
        "bamboo": "node node_modules/mocha/bin/mocha -R mocha-bamboo-reporter > mocha.json"
    }
    
In Bamboo, create an "npm task" with command `run-script bamboo`
Then, in Bamboo add a "Parse mocha results" task which runs afterwards to parse the results from mocha

If you don't do a full checkout on each build, make sure you add a task to delete mocha.json BEFORE the `npm run-script bamboo` task (I use a script task that runs `rm -f mocha.json`)
## License

Copyright (C) 2013 - 2013 Atlassian Corporation Pty Ltd. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.