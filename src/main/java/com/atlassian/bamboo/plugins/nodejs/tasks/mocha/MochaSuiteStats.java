package com.atlassian.bamboo.plugins.nodejs.tasks.mocha;

import java.util.Date;

public class MochaSuiteStats {
	int suites;
	int tests;
	int passes;
	int pending;
	int failures;
	Date start;
	Date end;
	int duration;
}