package com.atlassian.bamboo.plugins.nodejs.tasks.mocha;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;

public class MochaReportTask implements TaskType {
	private final TestCollationService testCollationService;
	
	public MochaReportTask(TestCollationService testCollationService)  {
		this.testCollationService = testCollationService;
	}
	@Override
	@NotNull
	public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException {
		final String testFilePattern = "mocha.json";
		testCollationService.collateTestResults(taskContext, testFilePattern, new MochaReportCollector());
		return TaskResultBuilder.create(taskContext).checkTestFailures().build();
	}

}
